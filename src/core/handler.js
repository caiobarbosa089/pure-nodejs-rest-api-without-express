const { parse } = require('querystring');
const helpers = require('../utils/helpers');

module.exports = async (req, res, routes) => {
	// Static HTML salutation for root route, by default
	if (req.url === '/' && req.method === 'GET') {
		const data = `<div>Welcome, server is running at port ${process.env.PORT}</div>`;
		res.writeHead(200, {'Content-Type': 'text/html', 'Content-Length': data.length});
		res.write(data);
		res.end();
	} else {
		/**
		 * Search for and return valid matched route
		 * @type {Boolean}
		 */
		const route = routes.find(route => {
			// Verify if mapped rout matches with request method
			const methodMatch = route.method === req.method;
			// Invalid path match by default
			let pathMatch = false;
			// Match path with RegEx if it's set in route mapping
			if (typeof route.path === 'object') pathMatch = req.url.match(route.path);
			// Or else, just test string equality
			else pathMatch = route.path === req.url;
			// Return based in short-circuit evalutation
			return pathMatch && methodMatch;
		});

		// Querystring parameter
		let param = null;

		// With found route, extract param if path is RegEx
		if (route && typeof route.path === 'object') param = req.url.match(route.path)[1];

		// Continues from a valid route
		if (route) {
			let body = null;
			// For POST and PUT methods, we need to extract posted information from body chunks
			if (req.method === 'POST' || req.method === 'PUT')
				body = await getPostData(req).catch(helpers.error(res, 'Can\'t read posted information', 500));
			// Return handled information directly from route handler
			return route.handler(req, res, param, body);
		// Route not found
		} else return helpers.error(res, ' Endpoint not found', 404);
	}
}

/**
 * Mount POST data from chunks returning promise
 * @type {Object}
 * @return {Promise}
 */
const getPostData = (req) => {
	return new Promise((resolve, reject) => {
		try {
			let body = '';
			req.on('data', chunk => body += chunk.toString());
			req.on('end', () => resolve(body));
		} catch (e) {
			reject(e);
		}
	});
}
