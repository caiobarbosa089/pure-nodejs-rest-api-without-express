// Set Headers -------------------------------------------------------------
const addHeaders = res => {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, HEAD, OPTIONS, TRACE, PATCH');
	res.setHeader('Content-Type', 'application/json');
}

/**
 * Return erro to the requester with it's respective headers, proper status and data
 * @param  {Object} res              Response received from server routing control
 * @param  {String} [error='Unknown error occurred'] Error message (optional)
 * @param  {Number} [statusCode=500] Status code (optional)
 */
module.exports.error = (res, error = 'Unknown error occurred', statusCode = 500) => {
	addHeaders(res);
	res.statusCode = statusCode;
	res.end(JSON.stringify({ status: 'fail', error }, null, 3))
}

/**
 * Return success to the requester with it's respective headers and data
 * @param  {Object} res         Response received from server routing control
 * @param  {Object} [data=null] Data (optional with null by default)
 */
module.exports.success = (res, data = null) => {
	addHeaders(res);
	res.statusCode = 200;
	res.end(JSON.stringify({ status: 'success', data }, null, 3));
}
