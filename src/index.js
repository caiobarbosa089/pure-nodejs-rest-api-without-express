// Load Environment Variables (.env) ----------------------------------------
require('dotenv').config();

// Dependencies -------------------------------------------------------------
const http = require('http');
const EventEmitter = require('events');
const helpers = require('./utils/helpers');

// Event Emmiter ------------------------------------------------------------
const ee = new EventEmitter();
ee.on('log', (...args) => console.log(`${new Date().toLocaleDateString('pt-br')} ${new Date().toLocaleTimeString('pt-br')}`, args));

// Safely exceptions handling -----------------------------------------------
process.on('uncaughtException', err => ee.emit('log', 'Uncaught exception has been detected', err));
process.on('unhandledRejection', err => ee.emit('log', 'Unhandled exception has been detected', err));

// Server------- ------------------------------------------------------------
const server = http.createServer(async (req, res) => {
	await require('./core/handler')(req, res, require('./core/routes')); //Orchestrate server processing properly
});
server.listen(process.env.PORT, () => ee.emit('log', `Localhost listening to port ${process.env.PORT}`));
