# pure-nodejs-rest-api-without-express

## Preview
RESTful API handling MongoDB records using **pure NodeJS** - **without Express**.

## Important topics
- Server port is coming environment variables - It's added into **.env** file that loads in server by **dotenv** package;
- Routes mapped into configuration file to identify the **route paths, method and handler (bind to Controller method)**;
- **Handler** to guide server processing based in **req**, **res** and defined rules;
- Handler file has important tasks too like **mounting posted data information based in chunks for PUT and POST requests**;
- Working with **Async/Await** within Node life-cycle;
- **Helpers** to provide standardized success and error responses, with proper headers;
- Intercepting **unhandled** and **uncaught** exceptions globally.

## Requirements
Be sure you have **node** and **npm** installed, you can download easily in the [official website](https://nodejs.org/en/download/).

After the installation you can open the terminal and test both node and npm typing
```
node -v
```
This way you'll get node's respective version if the installation was fully OK.
```
npm -v
```
This way you test the npm, the same way you test node.

If you have both messages you are ready to go! :)

The **npm** is used to install and manage project dependencies and run it by command line, **becomes with node instalation**.


## How to use
With everything configured, open your terminal and go to the `src` folder under the root of the repository you cloned.

Run `npm i` to intall all the dependencies. Wait a little bit - This process may take a while.

Run `npm start` to start the application and that's all.

To stop the running application press `ctrl + c` and `S` after.


## Developed by
Rodrigo Quiñones Pichioli - since July 24, 2019